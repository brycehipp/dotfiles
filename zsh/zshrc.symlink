# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh
DOTFILE_HOME=$HOME/Development/dotfiles/_mine
PLUGIN_HOME=$HOME/Development/_commands/zsh/plugins

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="avit"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(gitfast brew rails gem nyan osx pip sudo sublime aws bower npm history)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
#export PATH=/usr/local/bin:$PATH:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/Users/bryce/.rvm/gems/ruby-2.0.0-p247/bin:/Users/bryce/.rvm/gems/ruby-2.0.0-p247@global/bin:/Users/bryce/.rvm/rubies/ruby-2.0.0-p247/bin:/Users/bryce/.rvm/bin:/Users/bryce/.rvm/bin
export PATH=/usr/local/bin:$HOME/.rbenv/bin:$PATH:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/local/bin/

# Startup rbenv
eval "$(rbenv init - zsh)"

# Startup nvm
source ~/.nvm/nvm.sh

# Safeguards
unsetopt RM_STAR_SILENT
setopt RM_STAR_WAIT

# antigen zsh plugin manager
source "$HOME/.antigen/antigen.zsh"
#antigen-use oh-my-zsh
#antigen-bundle arialdomartini/oh-my-git
#antigen theme arialdomartini/oh-my-git-themes oppa-lana-style
antigen-apply

# all of our zsh files
typeset -U config_files
config_files=($DOTFILE_HOME/**/*.zsh)

# load everything but the path and completion files
for file in ${${config_files:#*/path.zsh}:#*/completion.zsh}
do
  source $file
done

# initialize autocomplete here, otherwise functions won't be loaded
autoload -U compinit
compinit

# load every completion after autocomplete loads
for file in ${(M)config_files:#*/completion.zsh}
do
  source $file
done

unset config_files
